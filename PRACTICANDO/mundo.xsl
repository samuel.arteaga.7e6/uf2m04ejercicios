<?xml version="1.0" encoding="UTF-8"?> <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<head>
				<title>Mundo</title>
			</head>
			<body>
				<h1>Continentes</h1>
				<xsl:for-each select="mundo/continente">
					<xsl:element name="table">
						<xsl:attribute name="style">border: 1px double <xsl:value-of select="nombre/@color"/>; border-collapse: separate;</xsl:attribute>
						<tr style="{concat('border: 1px double ', nombre/@color)}">
							<td style="{concat('border: 1px double ', nombre/@color)}"><b>Bandera</b></td>
							<td style="{concat('border: 1px double ', nombre/@color)}"><b>País</b></td>
							<td style="{concat('border: 1px double ', nombre/@color)}"><b>Capital</b></td>
						</tr>
						<xsl:for-each select="paises/pais">
							<tr style="{concat('border: 1px solid ', ../../nombre/@color)}">
								<td style="{concat('border: 1px solid ', ../../nombre/@color)}">
									<xsl:element name="img">
										<xsl:attribute name="class">Imagen Bandera</xsl:attribute>
										<xsl:attribute name="src"><xsl:value-of select="foto"/></xsl:attribute>
									</xsl:element>
								</td>
								<td style="{concat('border: 1px solid ', ../../nombre/@color)}"><xsl:value-of select="nombre"/></td>
								<td style="{concat('border: 1px solid ', ../../nombre/@color)}"><xsl:value-of select="capital"/></td>
							</tr>
						</xsl:for-each>
					</xsl:element>
				</xsl:for-each>
			</body>
		</html>
	
</xsl:template>
</xsl:stylesheet>
	
		
	
