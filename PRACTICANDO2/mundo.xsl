<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
     <html>
		<head><title>Examen UF2</title></head>
		<body>
			<h1>Continentes</h1>
			<xsl:for-each select="mundo/continente">
				<table border="2">
					<p style="font-weight:bold"><xsl:value-of select="nombre"></xsl:value-of></p>
					<tr style="nombre/@color">
						<td>Bandera</td>
						<td>Pais</td>
						<td>Capital</td>
					</tr>
					<xsl:for-each select="paises/pais">
					<tr>
						<td>
							<xsl:element name="img">
								<xsl:attribute name="src">
									<xsl:value-of select="foto"></xsl:value-of>
								</xsl:attribute>
							</xsl:element>
						</td>
						<td><xsl:value-of select="nombre"></xsl:value-of></td>
						<td style=""><xsl:value-of select="capital"></xsl:value-of></td>
					</tr>
					</xsl:for-each>
				</table>
			</xsl:for-each>
		</body>
     </html>
   </xsl:template>
</xsl:stylesheet>
